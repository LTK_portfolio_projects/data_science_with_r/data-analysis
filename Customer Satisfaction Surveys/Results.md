
# Professional Analysis of Customer Satisfaction Data

## Store Performance Analysis

### Analysis of p-values:
The highest p-value is seen in Product Quality Rating (0.7894), suggesting that customer perceptions of product quality are remarkably consistent across different locations.

The lowest p-value is observed in Cleanliness Rating (0.1861), but it is still well above the threshold for statistical significance.

The uniformity in customer satisfaction ratings across different store locations is evident. This suggests that the store chain maintains consistent standards in product quality, staff friendliness, cleanliness, and overall satisfaction.

### Visual plots results:

Across all locations, the majority of ratings are 4 or 5 (light and dark blue), implying generally high overall satisfaction.
Perth has the highest proportion of ratings at 5 (dark blue), while the Sunshine Coast has the highest number of ratings at 1 (red).

The plots suggest that while overall satisfaction is generally high, there are areas of improvement for each location, especially in cleanliness for the Sunshine Coast and product quality in Melbourne. It also suggests that despite the Kruskal-Wallis test results indicating no significant differences, individual store locations may still have specific areas where they could focus on improving customer experience.

### Summary
- Uniform customer satisfaction ratings across various store locations.
- No significant outperformance or underperformance in specific areas across locations.

### Key Results

Kruskal Wallis tests results comparing each Ratings across Store Locations (Table 1)

| Metric                      | Kruskal-Wallis Chi-squared | p-value |
|-----------------------------|----------------------------|---------|
| Product Quality Rating      | 1.7071                     | 0.7894  |
| Staff Friendliness Rating   | 5.5494                     | 0.2354  |
| Perthleanliness Rating      | 6.1804                     | 0.1861  |
| Overall Satisfaction Rating | 3.6409                     | 0.4568  |


![plot1](./plots/Customer Satisfaction Rating By Store Location.jpeg)
Customer Satisfaction Rating By Store Location Plot (Fig.1)


![plot2](./plots/Boxplot of Overall Satisfaction Rating Rating across Store Location.png)
Overall Satisfaction Rating Rating across Store Location boxplot (Fig.2)


## Age Group Preferences

### Analysis of P-values:
The Kruskal-Wallis test results indicate that the satisfaction levels regarding product quality, staff friendliness, cleanliness, and overall experience are similar across different age groups. This suggests that the store's appeal and performance are consistent and not significantly influenced by the age of the customers.

### Visual plots results:
Across both visuals, the 56+ age group appears to be the most satisfied overall, while the 18-25 age group seems to be the least satisfied.

The box plot and the bar chart both suggest that there are differences in satisfaction levels across age groups, with older customers tending to be more satisfied than younger ones.

The bar chart shows specific areas of dissatisfaction within age groups, such as product quality for the 18-25 age group and cleanliness for the 36-45 age group, which could be targeted for improvements.

### Summary
- Similar satisfaction levels across different age groups.
- No significant preference difference in product quality, staff friendliness, or cleanliness among age groups.

### Key Results

Kruskal Wallis tests results comparing each Ratings across Age Groups (Table 2)

| Metric | Kruskal-Wallis Chi-squared | p-value |
| ------ |----------------------------|---------|
| Product Quality Rating | 1.2362                     | 0.8721  |
| Staff Friendliness Rating | 3.4814                     | 0.4807  |
| Perthleanliness Rating  | 3.1592                     | 0.5315  |
| Overall Satisfaction Rating | 5.0075                     | 0.2865  |


![plot3](./plots/Customer Satisfaction Rating By Age Group.jpeg)
Customer Satisfaction Rating By Age Group Plot (Fig.3)

![plot4](./plots/Boxplot of Overall Satisfaction Rating Rating across Age Group.png)
Overall Satisfaction Rating Rating across Age Group boxplot (Fig.4)

## Overall Satisfaction Correlations
### Analyse of Correlation Matrix
Concluding Results from Table 3
- Product Quality has a moderate positive correlation with Overall Satisfaction (0.4888), suggesting that as product quality increases, so does overall satisfaction. 

- Staff Friendliness has a slightly stronger positive correlation with Overall Satisfaction (0.5044) than product quality, indicating its significant impact on customer satisfaction.

- The correlations between Cleanliness and other factors are weak, with the strongest being a negligible positive relationship with Staff Friendliness (0.0677).

- Store Location and Age Group have very weak correlations with all other factors, suggesting these variables have little association with customer satisfaction in this dataset.

###  Analyse of Correlation Significance (P-values)
Concluding Results from Table 4
- The p-values for correlations between Product Quality and Overall Satisfaction, and Staff Friendliness and Overall Satisfaction are both less than 0.0001, indicating these correlations are statistically significant.

- The p-values for most other correlations are much higher, notably exceeding the conventional significance level of 0.05, suggesting that the associations are not statistically significant, and any observed correlation could be due to chance.

### Sumamry:
- Staff Friendliness and Product Quality are both important factors that are statistically significantly correlated with Overall Satisfaction.

- Cleanliness seems to have a minimal impact on overall satisfaction, or the measure of cleanliness used may not capture its effect on satisfaction adequately.

- Store Location and Age Group appear to have little to no significant correlation with customer satisfaction metrics in this dataset.


### Key Results

#### Spearman Correlation Results


![plot5](./plots/Correlation plot across all variables.jpeg)
Fig.5 Spearman Correlation test results Coefficients Matrix for Customer Satisfaction Metrics (Comparing correlation across all variables)

Table 3 P-Value Matrix Spearman Correlation test results Coefficients (Comparing correlation across all variables)

| Factor | Product Quality | Staff Friendliness | Cleanliness | Overall Satisfaction | Store Location | Age Group |
| ------ | --------------- | ------------------ | ----------- | -------------------- | -------------- | --------- |
| Product Quality | NA | 0.1007 | 0.5865 | <0.0001 | 0.4853 | 0.8749 |
| Staff Friendliness | 0.1007 | NA | 0.1307 | <0.0001 | 0.9700 | 0.8620 |
| Cleanliness | 0.5865 | 0.1307 | NA | 0.7519 | 0.1022 | 0.2317 |
| Overall Satisfaction | <0.0001 | <0.0001 | 0.7519 | NA | 0.5842 | 0.7469 |
| Store Location | 0.4853 | 0.9700 | 0.1022 | 0.5842 | NA | 0.8826 |
| Age Group | 0.8749 | 0.8620 | 0.2317 | 0.7469 | 0.8826 | NA |


## Linear Regression model analysis
### Residuals
The residuals of the model (differences between the observed values and the model's predicted values) show that:

- The smallest residual (Min) is -1.10345 and the largest (Max) is 0.42016, indicating that the model's predictions are relatively close to the actual observations.

- The median residual is very close to zero (0.01503), suggesting that the model does not systematically overpredict or underpredict the overall satisfaction.

### Coefficients:
- The Intercept has a value of 1.6021829, which would be the model's prediction for Overall Satisfaction Rating when all independent variables are at zero, which is a hypothetical baseline in this context.

- Product Quality Rating has a coefficient of 0.2857405, indicating that for each unit increase in product quality rating, there is an estimated increase of 0.2857405 in the overall satisfaction rating, holding other variables constant.

- Staff Friendliness Rating has a slightly higher coefficient of 0.3097307, suggesting it has a slightly stronger positive effect on overall satisfaction than product quality.

- Cleanliness Rating has a coefficient of 0.0002991, which is statistically insignificant given its p-value of 0.978. This means changes in cleanliness rating have no significant effect on overall satisfaction in this model.

### Statistical Significance:
- The p-values for the intercept, product quality, and staff friendliness are all less than 2e-16, which is much less than 0.05, indicating these coefficients are highly statistically significant.

- The significance codes indicate that the coefficients for product quality and staff friendliness are both very significant (indicated by "***"), while cleanliness is not (p-value of 0.978).

### Model Fit:
- The Residual standard error (RSE) of 0.2752 is relatively small, suggesting a good fit of the model to the data.

- The Multiple R-squared value of 0.5618 means that approximately 56.18% of the variability in Overall Satisfaction Rating can be explained by the model. This is a moderately strong fit.

- The F-statistic is 212 on 3 and 496 degrees of freedom, with a p-value less than 2.2e-16, which shows that the model is statistically significant overall.

### Conclusion:
The model suggests that both Product Quality and Staff Friendliness are important and significant predictors of Overall Satisfaction, with Staff Friendliness having a slightly stronger influence. Cleanliness does not appear to be a significant predictor of Overall Satisfaction in this model. This model is robust and explains over half of the variability in the Overall Satisfaction Rating, which is quite substantial for social science data.

Table 4 & 5  Linear regression coefficients result tables

| Coefficient                    | 1.Estimate   | Std. Error  | t value   | Pr(>t) |
|------------------------------|-------------|------------|---------|--------|
| Intercept                    | 1.6021829   | 0.0993179  | 16.132  | <2e-16 |
| Product_Quality_Rating       | 0.2857405   | 0.0158285  | 18.052  | <2e-16 |
| Staff_Friendliness_Rating    | 0.3097307   | 0.0163452  | 18.949  | <2e-16 |
| Perthleanliness_Rating       | 0.0002991   | 0.0107668  | 0.028   | 0.978  |


| Measure           | Value   |
|-------------------|---------|
| Residual Std. Error | 0.2752  |
| Multiple R-squared | 0.5618  |
| Adjusted R-squared | 0.5591  |
| F-statistic       | 212     |
| Degrees of Freedom (DF) | 3 and 496 |
| p-value           | <2.2e-16 |


Fig.6 Linear Regression plot Product Quality Rating vs Overall Satisfaction Rating
![plot6](./plots/Linear Regression plot Product Quality Rating vs Overall Satisfaction Rating.jpeg)

Fig.7 Linear Regression plot Staff Friendliness Rating vs Overall Satisfaction Rating
![plot7](./plots/Linear Regression plot Staff Friendliness Rating vs Overall Satisfaction Rating.jpeg)

Fig.8 Linear Regression plot Perthleanliness Rating vs Overall Satisfaction Rating
![plot8](./plots/Linear Regression plot Perthleanliness Rating vs Overall Satisfaction Rating.jpeg)


