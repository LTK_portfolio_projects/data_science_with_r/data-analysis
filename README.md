# Analyzing Customer Satisfaction Across Store Locations

## Objective
To assess and understand customer satisfaction levels across various store locations, focusing on different age groups. This project involves analyzing customer feedback data from multiple store locations. The dataset contains customer ratings for product quality, staff friendliness, cleanliness, and overall satisfaction. The survey participants are categorized into different age groups, providing an opportunity to analyze preferences and satisfaction levels across a diverse customer base.

## Research Questions

### Store Performance Analysis
- How do customer satisfaction ratings vary across different store locations?
- Are there any locations that consistently outperform or underperform in specific areas (e.g., product quality, staff friendliness)?

### Age Group Preferences
- Does customer satisfaction differ among various age groups?
- Are there particular aspects (like product quality or cleanliness) that are more important to certain age groups?

### Overall Satisfaction Correlations
- Which factors (product quality, staff friendliness, cleanliness) most strongly correlate with overall satisfaction ratings?
- Are there any unexpected trends or outliers in how these factors affect overall satisfaction?

### Improvement Opportunities
- Based on the data, what recommendations can be made to improve customer satisfaction?
- Are there specific areas where certain store locations can focus their improvement efforts?

### Longitudinal Trends (if applicable)
- If the data spans over a period, are there any noticeable trends in customer satisfaction over time?
- How do seasonal variations (if any) impact customer satisfaction ratings?
